from django.apps import AppConfig


class BostonappConfig(AppConfig):
    name = 'bostonapp'
